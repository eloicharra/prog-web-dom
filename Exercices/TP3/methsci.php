<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RSS METHSCI</title>
	<link rel="stylesheet" type="text/css" href="methsci.css">
</head>
<body>

<?php
require_once('vendor/dg/rss-php/src/Feed.php');
header('Content-Type: text/html; charset=utf-8');

if (!ini_get('date.timezone')) {
	date_default_timezone_set('Europe/Prague');
}
$rss = Feed::loadRss('radiofrance-podcast.net/podcast09/rss_14312.xml');

?>



<h1><?php echo htmlspecialchars($rss->title) ?></h1>

<p><i><?php echo htmlspecialchars($rss->description) ?></i></p>

<table>
<tr>
	<td>Titre</td>
	<td>Date</td>
	<td>Lecture</td>
	<td>Durée</td>
	<td>Média</td>
</tr>
<?php foreach ($rss->item as $item): ?>
<tr>
	<td>
		<h2><a href="<?php echo htmlspecialchars($item->url) ?>"><?php echo $item->title ?></a></h2>
	</td>
	<td>
		<?php echo date('j.n.Y H:i', (int) $item->timestamp) ?>
	</td>
</tr>
<?php endforeach ?>
</table>



<?php foreach ($rss->item as $item): ?>
	<h2><a href="<?php echo htmlspecialchars($item->url) ?>"><?php echo htmlspecialchars($item->title) ?></a>
	<small><?php echo date('j.n.Y H:i', (int) $item->timestamp) ?></small></h2>

	<?php if (isset($item->{'content:encoded'})): ?>
		<div><?php echo $item->{'content:encoded'} ?></div>
	<?php else: ?>
		<p><?php echo htmlspecialchars($item->description) ?></p>
	<?php endif ?>
<?php endforeach ?>