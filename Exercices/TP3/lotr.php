<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Movie infos</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
require_once("../../Helpers/tp3-helpers.php");

function getCollectionInfo()
{
    $collectionInfo = json_decode(tmdbget("search/collection", ["query" => "the lord of the rings"]));
    $collectionId = $collectionInfo->results[0]->id;
    return json_decode(tmdbget("collection/" . $collectionId));

}

function getInfo($rawData, $movieID)
{

    $res = array();
    $res["title"] = $rawData->title;
    $res["original_language"] = $rawData->original_language;
    $res["original_title"] = $rawData->original_title;
    $res["release_date"] = $rawData->release_date;
    if (isset($rawData->tagLine))
        $res["tagline"] = $rawData->tagline;
    $res["id"] = $movieID;
    $res["link"] = "https://www.themoviedb.org/movie/" . $movieID . "-" . str_replace(" ", "-", $res["original_title"]);
    $res["imageLink"] = "https://image.tmdb.org/t/p/w154/" . $rawData->poster_path;

    return $res;
}

function getActors($movieID, $actors)
{
    $rawData = json_decode(tmdbget("movie/" . $movieID . "/credits"));
    foreach ($rawData->cast as $person) {
        $currentActor = array();
        if ($person->{"known_for_department"} == "Acting") {
            if (isset($actors[$person->id])) {
                $actors[$person->id]["count"] += 1;
            } else {
                $currentActor["id"] = $person->id;
                $currentActor["name"] = $person->name;
                $currentActor["char"] = $person->character;
                $currentActor["count"] = 1;
                $actors[$person->id] = $currentActor;
            }
        }
    }
    return $actors;
}

function printInfos($movies)
{
    echo "<table>";

    foreach (array_keys($movies[0]) as $key) {
        if ($key == "original_language")
            continue;
        echo "<tr>";
        if ($key == "link") {
            foreach ($movies as $movie) {
                echo "<td><a href=" . $movie[$key] . ">" . $movie[$key] . " </a></td>";
            }

        } else if ($key == "imageLink") {
            foreach ($movies as $movie) {
                echo "<td><img src=" . $movie[$key] . "></img></td>";
            }
        } else {
            foreach ($movies as $movie) {
                echo "<td>" . $movie[$key] . "</td>";
            }
        }
        echo "</tr>";

    }
    echo "</table>";

}

function printActors($actors) {

    echo "<table>";

    echo "<tr>";
    echo "<th>";
    echo "Acteur";
    echo "</th>";
    echo "<th>";
    echo "Personnage";
    echo "</th>";
    echo "<th>";
    echo "Nombre d'apparitions";
    echo "</th>";
    echo "</tr>";

    foreach ($actors as $actor) {
        echo "<tr>";
        foreach (array_keys($actor) as $key) {
            if($key == "id")
                continue;
            echo "<td>";
            if($key == "name") {
                echo "<a href='/PW/prog-web-dom/Exercices/TP3/moviesOfActor.php?actor=" . $actor["id"] ."'> " . $actor[$key] . "</a>";
            } else {
                echo $actor[$key];
            }
            echo "</td>";

        }
        echo "</tr>";

    }
    echo "</table>";
}

function filterHobits($actors) {
    $res = array();
    foreach($actors as $actor) {
        if(str_contains($actor["char"], "Hobbit")) {
            $res[] = $actor;
        }
    }
    return $res;
}
$collectionData = getCollectionInfo();
$movies = array();
$actors = array();
foreach ($collectionData->parts as $movie) {
    $movies[] = getInfo($movie, $movie->id);
    $actors = getActors($movie->id, $actors);
}
printInfos($movies);
echo "<br/>";
printActors($actors);
echo "<br/>";
printActors(filterHobits($actors))

?>

</body>
</html>
