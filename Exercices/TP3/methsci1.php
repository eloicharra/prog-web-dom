<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>methsci</title>
	<link rel="stylesheet" type="text/css" href="methsci.css">
</head>
<body>

<?php
require_once('vendor/dg/rss-php/src/Feed.php');

$rss = Feed::loadRss('radiofrance-podcast.net/podcast09/rss_14312.xml');

//var_dump($rss);

$items=array();

$items["titres"]=array();
$items["infos"]["titre"]=array();
$items["infos"]["url"]=array();
$items["dates"]=array();
$items["duree"]=array();
$items["audio"]=array();


foreach ($rss->item as $item){
	$items["infos"]["titre"][]=$item->title;
	$items["infos"]["url"][]=$item->link;
	$items["dates"][]=date('j.n.Y H:i', (int) $item->timestamp);
	$items["duree"][]=$item->{'itunes:duration'};
	$items["audio"][]=$item->enclosure->attributes()["url"];
}

echo "<table>";
echo "<tr>";
echo "<td></td>";
echo "<td>Titre</td>";
echo "<td>Date</td>";
echo "<td>Durée</td>";
echo "<td>Lien MP3</td>";
echo "<td>Téléchargement</td>";
echo "</tr>";

for($i=0; $i < count($items["dates"]); $i++) 
{
	echo "<tr>";
	foreach(array_keys($items) as $key) 
	{
		echo "<td>";
		if($key=="audio")
		{
			header("Content-disposition; attachment; filename=test.mp3");
			header("Type de contenu : audio/mpeg");
			readfile("exemple.mp3");
			// echo "<audio controls src =".$items[$key][$i]."></audio>";
			// echo "</td><td>";
			// echo "<a href=".$items[$key][$i]." download";
			// echo "=";
			// echo "> Télécharger le mp3</a>";
		}
		elseif ($key=="infos") 
		{	
			echo "<a href =".$items[$key]["url"][$i].">".$items[$key]["titre"][$i]."</a>";
		}
		elseif($key=="dates" || $key=="duree")
		{
			echo $items[$key][$i];
		}
		echo "</td>";
	}

	echo "</tr>";

}

?>

