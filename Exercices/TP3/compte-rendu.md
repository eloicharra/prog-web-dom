% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : TMDB

## Participants 

* Eloi Charra
* Paul Blanchet

# 1

Le format de réponse est le JSON, il s'agit du film Fight Club. Avec le paramètre 
`language=fr`, on peut les champs traduits en français comme le titre et la description.

# 2

Nous utilisons le service curl `curl` comme ceci :
`curl http://api.themoviedb.org/3/movie/550?api_key=ebb02613ce5a2ae58fde00f4db95a9c1&language=fr`
Nous avons écrit un programme minimal dans le fichier `cliapi.php` en utilisant `tp3-helper.php`.

# 3

Nous récupérons d'abord les données bruts avec `tp3-helper.php` en donnant en paramètre
un identifiant de film qui sera passé en paramètre GET dans l'URL de la page.
Nous traitons ensuite les données pour ne garder que son titre, son titre original, sa _tagline_,
sa description et le lien vers la page TMDB. Nous utilisons ensuite la fonction `printInfos`
pour afficher toutes ces informations de manière à être un minimum lisible.

# 4

Nous récupérons une "première" version des données qui sera celle en anglais. À partir de
ce jeu de données, nous récupérons la langue originale qui est donnée dans le JSON.
Nous récupérons donc ensuite les données dans la langue originale ainsi qu'en français.
La fonction `printInfos` affiche enfin toutes ces données sous la forme d'un tableau.

# 5

Nous récupérons une information de plus se trouvant dans le JSON qui est `poster_path`.
Nous concaténons cet identifiant de _poster_ à ce lien `https://image.tmdb.org/t/p/w154/`
pour récupérer une image de taille résonable venant du site TMDB.
Il suffit enfin d'ajouter une ligne au tableau pour afficher cette image.

# 6

Nous faisons un appel à `tp3-helper.php` avec comme paramètres `search/collection", ["query" => "the lord of the rings"]`.
Nous cherchons une collection de film avec comme paramètre de recherche _lors of the rings_.
Cette collection nous renvoie 3 films que nous traitons. Il suffit enfin d'afficher les résultats
dans un tableau.

# 7

Pour chaque film de la collection, nous récupérons les acteurs en utilisant le paramètre `/credit`
à la fin de la requête `curl`. On ne garde que les personnes ayant le rôle `Acting`, et pour ceux-là,
on récupère leur nom et leur personnage joué dans le film. Nous appelons cette fonction
qui permet de traiter les données 3 fois (une fois pour chaque film) avec le même tableau associatif en paramètre.
Ce tableau comporte les identifiants des acteurs pour les clés et un tableau associatif pour la valeur associée.
Pour chaque acteur, on regarde si son identifiant apparaît déjà dans le tableau. Si c'est le cas,
on incrémente de 1 le champ `count` qui permet de compter le nombre apparitions de cet
acteur dans les 3 films.

# 8

Nous filtrons la liste des acteurs en regardant le champ `char` (character (personnage))
en vérifiant que le mot _Hobbit_ apparaît. On ne garde que les acteurs vérifiant cette
condition et affichons le résultat.

# 9

Nous récupérons l'identifiant de l'acteur lorsque nous créons notre tableau contenant
tous les acteurs. Nous remplaçant l'affichage du nom de l'acteur par un hyperlien menant sur la 
page `moviesOfActor.php` avec en paramètre GET dans l'URL, son identifiant.
Dans la page `moviesOfActor`, nous récupérons tous les films auxquels il a participé via
les paramètres `"person/" . $actorID . "/movie_credits")`. On traite ensuite les données pour
ne garder que le nom du film et le nom du personnage incarné pour enfin afficher le tout
dans un tableau.

# 10

Nous ajoutons le paramètre `append_to_response => 'videos'` à la requête pour pouvoir
récupérer le lien du trailer. Le lien donné par l'API est un code pour une vidéo YouTube,
nous ajoutons une donnée dans notre tableau avec la clé `trailer` qui comporte
`https://youtube.com/embed/ . id_video` comme valeur. Ce lien représente une vidéo YouTube
intégrée dans une page web.
Nous ajoutons une ligne dans notre tableau pour afficher la vidéo youtube en utilisant un
`iframe` avec le lien YouTube comme valeur pour l'attribut `src`.

