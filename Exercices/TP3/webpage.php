<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Movie infos</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
require_once("../../Helpers/tp3-helpers.php");

$movieID = $_GET['movieID'] ?? 0;

function getMovieInfos($movieID, $language = null)
{
    $param = array("append_to_response" => "videos");
    if (!isset($language))
        $rawData = json_decode(tmdbget("movie/" . $movieID, $param));
    else {
        $param["language"] = $language;
        echo "<br/>";
        $rawData = json_decode(tmdbget("movie/" . $movieID, $param));
    }


    $res = array();
    $res["title"] = $rawData->title;
    $res["original_language"] = $rawData->original_language;
    $res["original_title"] = $rawData->original_title;
    $res["tagline"] = $rawData->tagline;
    $res["description"] = $rawData->overview;
    $res["link"] = "https://www.themoviedb.org/movie/" . $movieID . "-" . str_replace(" ", "-", $res["original_title"]);
    $res["imageLink"] = "https://image.tmdb.org/t/p/w154/" . $rawData->poster_path;
    $res["trailer"] = "https://youtube.com/embed/" . $rawData->videos->results[0]->key;
    return $res;
}

function printInfos($infosInAllLanguages)
{
    echo "<table>";

    foreach (array_keys($infosInAllLanguages[0]) as $key) {
        if ($key == "original_language")
            continue;
        echo "<tr>";
        if ($key == "link") {
            foreach ($infosInAllLanguages as $language)
                echo "<td><a href=" . $language[$key] . ">" . $language[$key] . " </a></td>";
        } else if($key == "imageLink") {
            foreach ($infosInAllLanguages as $language)
                echo "<td><img src=" . $language[$key] . "></img></td>";
        } else if($key == "trailer") {
            foreach ($infosInAllLanguages as $language)
                echo "<td><iframe height='315' width='420' src='" . $language[$key] . "'> </iframe></td>";
        } else {
            foreach ($infosInAllLanguages as $language)
                echo "<td>" . $language[$key] . "</td>";
        }
        echo "</tr>";

    }
    echo "</table>";

}

if ($movieID == 0 || !is_numeric($movieID)) {
    echo "<strong> Erreur lors du renseignement de l'identifiant du film (champ movieID en tant que paramètre GET dans l'URL)</strong>";
} else {
    $infosInAllLanguages = array();

    //Get infos in english
    $englishInfos = getMovieInfos($movieID);

    //Deduce original langue
    $originalLanguage = $englishInfos["original_language"];

    //Get french and original languages
    $originalInfos = getMovieInfos($movieID, $originalLanguage);
    $frenchInfos = getMovieInfos($movieID, "fr");

    array_push($infosInAllLanguages, $originalInfos, $englishInfos, $frenchInfos);

    printInfos($infosInAllLanguages);
}

?>

</body>
</html>
