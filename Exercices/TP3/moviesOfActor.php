<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Movie infos</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
require_once("../../Helpers/tp3-helpers.php");
$actorID = $_GET['actor'] ?? "";

function getActorName($actorID) {
    return json_decode(tmdbget("person/" . $actorID))->name;
}
function getData($actorID) {
    return json_decode(tmdbget("person/" . $actorID . "/movie_credits"));
}

function parseData($data) {
    $res = array();
    foreach($data->cast as $movie) {
        $res[] = ["movie" => $movie->original_title, "char" => $movie->character];
    }
    return $res;
}

function printActors($actors) {

    echo "<table>";

    echo "<tr>";
    echo "<th>";
    echo "Film";
    echo "</th>";
    echo "<th>";
    echo "Personnage";
    echo "</th>";
    echo "</tr>";

    foreach ($actors as $actor) {
        echo "<tr>";
        foreach ($actor as $field) {
            echo "<td>";
            echo $field;
            echo "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}
echo "<h1> Films et personnages pour " . getActorName($actorID) . "</h1>";
printActors(parseData(getData($actorID)));
