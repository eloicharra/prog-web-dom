<?php

require_once("../../Helpers/tp2-helpers.php");

function readFJSON($fileName) {
    $string = file_get_contents($fileName);
    $json = json_decode($string,true);
    $data = array();
    foreach ($json["features"] as $point) {
        $data[] = array("name" => $point["properties"]["AP_ANTENNE1"], "adr" => $point["properties"]["Antenne 1"], "lon" => $point["properties"]["longitude"], "lat" => $point["properties"]["latitude"]);
//        foreach ($point as $o) {
//            print_r($o);
//            if(!is_null($o))
//        }
    }
    return $data;
}
//readFJSON("borneswifi_EPSG4326_20171004.json");
//Q3
function echoNbPoints() {
    $data = file("borneswifi_EPSG4326_20171004_utf8.csv");
    echo "Il y a " . count($data) . " point d'accès wifi\n";
}

//echoNbPoints();

//Q4
function getData() {
    $data = array();
    $csvFile = file('borneswifi_EPSG4326_20171004_utf8.csv');
    $keys = ["name", "adr", "lon", "lat"];
    foreach ($csvFile as $line) {
        $data[] = array_combine($keys,str_getcsv($line));
    }
    array_shift($data);
    return $data;
}



//Q5
function distances() {
    $currLocal = geopoint(5.727342,45.191064);
    $data = getData();
    foreach ($data as $point) {
        echo "Le point " . $point['name'] . " à " . $point['adr'] . " est à " . distance($currLocal, $point) . "m \n";
    }

    echo "\n";   
}
function distancesLess200m() {
    $currLocal = geopoint(5.727342,45.191064);
    $data = getData();

    foreach ($data as $point) {
        $distance = distance($currLocal, $point);
        if($distance < 200)
            echo "Le point " . $point['name'] . " est à moins de 200m (" . distance($currLocal, $point) . ")\n";
    }
}

//distances();
//distancesLess200m();

//Q6
//$N = $argv[1];

function closestPoints($N, $lon, $lat, $data) {
    $currLocal = geopoint($lon,$lat);
    $res = array();
//    $data = getData();
    $newData = array();
    $distances = array();
    foreach ($data as $point) {
        $newData[] = array("name" => $point["name"], "adr" => $point["adr"], "lon" => $point["lon"], "lat" => $point["lat"], "distance" => distance($currLocal, $point));
    }
    $data = $newData;
    array_multisort(array_column($distances, 'distance'), SORT_DESC, $distances);
    print_r($distances);

    foreach ($data as $point) {
        if($N > 0) {
            $res[] = $point;
            $N--;
        }
    }
    return $res;
}
//Grenette:
//$lon = 5.727342;
//$lat = 45.191064;
//closestPoints($N, $lon, $lat);

//Q7

function getGeoCodage() {
    $data = getData();
    $newData = array();
    foreach($data as $point) {
        $url = "https://api-adresse.data.gouv.fr/reverse/?lat=" . $point['lat'] . "&lon=". $point['lon'];
        $response = json_decode(smartcurl($url, 0));
        $newData[] = array("name" => $point["name"], "adr" => $point["adr"], "lon" => $point["lon"], "lat" => $point["lat"], "realAdr" => $response->features[0]->properties->label);
    }
    $data = $newData;
    print_r($data);
}

//getGeoCodage();

