<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WebService</title>
</head>
<body>
<form method="get" action="webService.php">
    <label for="longitude">Longitude</label> <input type="text" id="longitude" name="longitude"/> <br />
    <label for="latitude">Latitude</label> <input type="text" id="latitude" name="latitude"/> <br />
    <label for="nb">Combien</label> <input type="text" id="nb" name="nb"/> <br />
    <input type="submit"/>
</form>

<?php

require_once("./traitement.php");

if(isset($_GET['longitude']))
    $lon = $_GET['longitude'];
else
    $lon = 0;

if(isset($_GET['latitude']))
    $lat = $_GET['latitude'];
else
    $lat = 0;

if(isset($_GET['nb']))
    $nb = $_GET['nb'];
else
    $nb = 0;

$data = readFJSON("borneswifi_EPSG4326_20171004.json");
$res = closestPoints($nb, $lon, $lat, $data);
print_r($res);

?>
</body>
</html>



