<?php
	require_once("libcalcul.php");

	$somme = intval($argv[1]);
	$taux = intval($argv[2]);
	$duree = intval($argv[3]);

	$cumul = cumul($somme, $taux, $duree);

	echo "cumul = $somme * ( 1 + $taux/100)^$duree = $cumul\n";
 ?>