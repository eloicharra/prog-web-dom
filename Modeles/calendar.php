<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CALENDRIER</title>
    <style type="text/css">
        table {
            border: 1px solid black;
        }

        td {
            padding: 30px;
        }
    </style>
</head>
<body>
<?php
session_start();
if(!isset($_SESSION["events"]))
    $_SESSION["events"] = array();
if (isset($_GET['month']))
    $month = $_GET['month'];
else
    $month = date("m");

if (isset($_GET['year']))
    $year = $_GET['year'];
else
    $year = date("y");

if (isset($_GET['day'])) {
    $rawDate = $_GET['day'];
    $date = explode("-", $rawDate);
    $month = $date[1];
    $year = $date[0];
}
else {
    $rawDate = 0;
    $date = 0;
    $dayEvent = -1;
}


if (isset($_GET['event'])) {
    $event = $_GET['event'];
    $_SESSION["events"][]= array("day" => $rawDate, "event" => $event);

}
else
    $event = "rien";

print_r($_SESSION["events"]);
?>
<form method="GET" action="calendar.php">
    <label for="day">Jour</label>
    <input type="date" id="day" name="day"/> <br/>
    <label for="event">Evenement</label>
    <input type="text" id="event" name="event"/> <br/>
    <input type="submit"/>
</form>


<?php

$lastMonday = date('j', strtotime('Monday this week ' . $year . '-' . $month . '-1'));
echo "<table>";
$dayCountForMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
$dayCountForLastMonth = cal_days_in_month(CAL_GREGORIAN, $month - 1, $year);
for ($day = 0; $day < $dayCountForLastMonth - $lastMonday + 1 + $dayCountForMonth; $day++) {
    if ($day == 0)
        echo "<tr>";
    if ($day % 7 == 0 && $day != 0) {
        echo "</tr>";
        echo "<tr>";
    }
    if ($day == $dayCountForLastMonth - $lastMonday + 1 + $dayCountForMonth)
        echo "<tr/>";
    echo "<td>";
    echo date("j l", mktime(0, 0, 0, $month - 1, $day + $lastMonday, $year));
    foreach($_SESSION["events"] as $value) {
        if(date("Y-m-d", mktime(0, 0, 0, $month - 1, $day + $lastMonday, $year))== $value["day"]) {
            echo "<br/>";
            echo $event;
        }
    }
    echo "</td>";
}
echo "</table>";

?>
</body>
</html>