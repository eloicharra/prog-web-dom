<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MULTIPLICATION</title>
	<link rel="stylesheet" type="text/css" href="mult.css">
</head>
<body>
<?php

if(isset($_GET['row']))
	$rowCount = $_GET['row'];
else
	$rowCount = 10;

if(isset($_GET['col']))
	$colCount = $_GET['col'];
else
	$colCount = 10;

if(isset($_GET['surlignee']))
	$surlignee = $_GET['surlignee'];
else
	$surlignee = -1;


echo "<table>";

for ($row = 0; $row < $rowCount; $row ++) {
	if($row == $surlignee)
		echo "<tr class='surlignee'>";
	else
   		echo "<tr>";

   for ($col = 1; $col <= $colCount; $col ++) {
        echo "<td>", ($col * $row), "</td>";
   }

   echo "</tr>";
}

echo "</table>";
?>
</body>
</html>



